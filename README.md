# child-pipelines-test

To test running GitLab CI child pipelines in the same stage.

See GitLab issue [Triggering multiple dynamically generated child pipelines in a single phase causes all but one to fail](https://gitlab.com/gitlab-org/gitlab/-/issues/212373).

Based on example CI script proposed [here](https://gitlab.com/gitlab-org/gitlab/-/issues/212373#note_489935325).
